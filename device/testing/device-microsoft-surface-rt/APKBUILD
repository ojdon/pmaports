# Reference: <https://postmarketos.org/devicepkg>
pkgname=device-microsoft-surface-rt
pkgdesc="Microsoft Surface RT"
pkgver=1
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="armv7"
# pmb:strict: avoid grub-related install error during build with pmbootstrap
options="!check !archcheck pmb:strict"
depends="postmarketos-base
	linux-postmarketos-grate
	mesa-dri-swrast
	libvdpau-tegra
	alsa-ucm-conf
"
makedepends="devicepkg-dev grub grub-efi"
subpackages="$pkgname-x11 $pkgname-phosh"
source="
	deviceinfo
	grub.cfg
	pointercal
	rootston.ini
"

build() {
	devicepkg_build $startdir $pkgname

	grub-mkimage \
		--prefix="/grub" \
		--output="bootarm.efi" \
		--format="arm-efi" \
		--compression="xz" \
		\
		disk \
		fat \
		gzio \
		iso9660 \
		linux \
		normal \
		part_gpt \
		part_msdos
}

package() {
	devicepkg_package $startdir $pkgname

	install -Dm644 "$srcdir"/pointercal \
		"$pkgdir"/etc/pointercal

	install -Dm644 "$srcdir"/grub.cfg \
		"$pkgdir"/boot/grub/grub.cfg
	install -Dm644 "$srcdir"/bootarm.efi \
		"$pkgdir"/boot/EFI/Boot/bootarm.efi
}

x11() {
	install_if="$pkgname xorg-server"
	depends="xf86-video-opentegra"
	mkdir "$subpkgdir"
}

phosh() {
	install_if="$pkgname postmarketos-ui-phosh"
	install -Dm644 "$srcdir"/rootston.ini \
		"$subpkgdir"/etc/phosh/rootston.ini
}

sha512sums="
37077b3b050285f73a5325e82e669f33813f57eac83aea37cf3b148812a5040b4a86cc10e52b351d4dbf9161ad2481fb856b0150142b4c68af71c88a1f1b43b6  deviceinfo
33be5b15e4a7b9318f58cec2c4837b3b2fae939aab8d967cfb5e326676fc144dad2a43991fbbb3c8b5f7741897300d6b92befa2e53e84e5ff7ab8e8b2590bbc5  grub.cfg
34f6eb789d4688985a262c519017e0712a90ca5acf89e6b3f39e33792a7982155261c6b4f0dbf41595b512da9dc7c9002adb69a550e80be349a374d086799916  pointercal
618284cdaccd09e60cb9a99afa337fcad7b3bd33f6422b9eae34175bc4516138d486cbb9f5735cafb325bc16cf362de16aeae7c77d334668c749afcfa557359b  rootston.ini
"
