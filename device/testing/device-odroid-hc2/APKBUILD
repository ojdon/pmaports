# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Dylan Van Assche <me@dylanvanassche.be>

pkgname=device-odroid-hc2
pkgdesc="ODROID HC2"
pkgver=0.3
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="armv7"
options="!check !archcheck"
depends="linux-odroid-hc2
	 networkmanager
	 postmarketos-base
	 u-boot-odroid
	 u-boot-tools
	"
makedepends="devicepkg-dev"
subpackages="$pkgname-nonfree-firmware:nonfree_firmware $pkgname-tools"
_commit="42ac93dcfbbb8a08c2bdc02e19f96eb35a81891a"
install="$pkgname.post-install $pkgname.post-upgrade"
source="deviceinfo uboot-script.cmd install-rootfs-hdd.sh"

build() {
	devicepkg_build $startdir $pkgname
	mkimage \
		-A arm \
		-O linux \
		-T script \
		-C none \
		-a 0 \
		-e 0 \
		-n postmarketos \
		-d "$srcdir"/uboot-script.cmd \
		"$srcdir"/boot.scr
}

package() {
	devicepkg_package $startdir $pkgname
	install -Dm644 "$srcdir"/boot.scr \
		"$pkgdir"/boot/boot.scr
}

nonfree_firmware() {
	pkgdesc="U-Boot and Realtek firmware, required for a bootable system"
	depends="firmware-odroid-hc2 linux-firmware-rtl_nic"
	mkdir "$subpkgdir"
}

tools() {
	pkgdesc="ODROID HC2 tools"
	depends="rsync e2fsprogs-extra"
	mkdir "$subpkgdir"
	install -Dm744 "$srcdir"/install-rootfs-hdd.sh \
		"$pkgdir"/usr/bin/install-rootfs-hdd
}

sha512sums="
d02702884d47c2c6b98f1ec194fc3a11b2c6559d416c112d41ddecfbbd84f0706adfc72e13e9943f130e548d987aa7d151f4f416443d15b7dff2375d4d067144  deviceinfo
48e76599c21dd0df595ad2f5a9fe9f71962b5c1f631c91ce6f9904fe393ee7bf7c0d71df54f08bdf7fc4ed9176d36abc2f887781159304f862bf996f2157a5bd  uboot-script.cmd
8273be5a8c5a7b9a27ae91a1581ca2d247d0fb45e24e25e46ba8272955d7fce31c62562c3fae402f6a66918c9c61d838de692cca11eea3cdfde6a91f478f2955  install-rootfs-hdd.sh
"
